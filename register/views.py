from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import NewUserForm, RoleForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

def homepage(request):
	return render(request=request, template_name="main/home.html")

def register_request(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		role = RoleForm(request.POST)
		if form.is_valid():
			if role.is_valid:
				form.save()
				role.save()
				login(request, form.save())
				messages.success(request, "Registration successful." )
				HttpResponseRedirect('/homepage/')
			messages.error(request, "Unsuccessful roles. Invalid information.")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm()
	role = RoleForm()
	return render(request=request, template_name="main/register.html", context={"register_form":form, "role_form":role,})

def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"You are now logged in as {username}.")
				return HttpResponseRedirect('/')
			else:
				messages.error(request,"Invalid username or password.")
		else:
			messages.error(request,"Invalid username or password.")
	form = AuthenticationForm()
	return render(request=request, template_name="main/login.html", context={"login_form":form})

def logout_request(request):
	logout(request)
	messages.info(request, "You have successfully logged out.") 
	return HttpResponseRedirect('/')

# JSON for register
def json_register(request):
    data = serializers.serialize("json", User.objects.all())
    return HttpResponse(data, content_type="application/json")

# API Call for POST Flutter
@csrf_exempt
def post_register(request):
    register_post = User.objects.create_user(
        # username = request.POST.get('username', None),
        # email = request.POST.get('email', None),
        # password1 = request.POST.get('password1', None),
        # password2 = request.POST.get('password2', None),
		# ini gw fix ya - brandon
		username = request.POST.get('username', None),
		email = request.POST.get('email', None),
		password = request.POST.get('password1', None),
    )
    register_post.save()