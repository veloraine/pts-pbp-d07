from django.http.response import HttpResponse
from django.shortcuts import render
from .models import Lyrics
from .forms import LyricsForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

# generate output page
def outputLyrics(request):
    lyrics = Lyrics.objects.all().values()
    response = {"lyrics": lyrics}
    return render(request, "lyrics_out_template.html", response)


# generate input form page
def inputLyrics(request):
    # request by POST
    form = LyricsForm(request.POST)

    if form.is_valid():
        # save input to database
        form.save()
        # redirect to output page
        return HttpResponseRedirect("/wash-your-lyrics/output")

    response = {"form": form}

    return render(request, "lyrics_in_template.html", response)

# API Call for POST Flutter
@csrf_exempt
def postMethod(request):
    submit_lyrics = Lyrics(
        Baris1 = request.POST.get('Baris1', None),
        Baris2 = request.POST.get('Baris2', None),
        Baris3 = request.POST.get('Baris3', None),
        Baris4 = request.POST.get('Baris4', None),
        Baris5 = request.POST.get('Baris5', None),
        Baris6 = request.POST.get('Baris6', None),
        Baris7 = request.POST.get('Baris7', None),
        Baris8 = request.POST.get('Baris8', None),
        Baris9 = request.POST.get('Baris9', None),
    )
    submit_lyrics.save()


# JSON response
def jsonLyrics(request):
    data = serializers.serialize("json", Lyrics.objects.all())
    return HttpResponse(data, content_type="application/json")