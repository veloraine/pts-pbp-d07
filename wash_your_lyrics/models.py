from django.db import models

lines = [
    "Baris1",
    "Baris2",
    "Baris3",
    "Baris4",
    "Baris5",
    "Baris6",
    "Baris7",
    "Baris8",
    "Baris9",
]

class Lyrics(models.Model):
    pass

for line in lines:
    Lyrics.add_to_class(
        line, models.CharField(max_length=100),
    )
