from django import forms
from .models import Lyrics

class LyricsForm(forms.ModelForm):
    class Meta:
        # form from Lyrics model
        model = Lyrics
        # all fields in model should be used
        fields = "__all__"
