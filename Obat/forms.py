from Obat.models import Obat
from django.forms import ModelForm
from django import forms

class ObatForm(ModelForm):
    class Meta:
        model=Obat
        fields=['name','info','image']