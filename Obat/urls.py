from django.urls import path
from django.urls.conf import include
from .views import add_obat,list_obat,jsonObat,postMethod


urlpatterns = [
    path('add_obat/', add_obat, name='add_obat'),
    path('list_obat/',list_obat,name='list_obat'),
    path('', list_obat, name='list_obat'),
    path('json',jsonObat, name='jsonObat'),
    path('post', postMethod, name='postMethod')
]