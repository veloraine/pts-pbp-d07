from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .forms import ObatForm
from .models import Obat
from django.http import HttpResponse, HttpResponseRedirect, request
from django.contrib.auth.decorators import login_required
from django.core import serializers


def add_obat(request):
    if request.method == 'POST':
        form=ObatForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            
        
    else:
        form=ObatForm()
    return render(request,'obat_form.html',{'form':form})

def list_obat(request):
    obats=Obat.objects.all()
    response={"obats":obats}
    return render(request,'obat_list.html',response)

#post method endPoint for flutter
@csrf_exempt
def postMethod(request):
    submit_obat = Obat(
         name = request.POST.get('name', None),
         info= request.POST.get('info',None)
    )
    submit_obat.save()

#JSON get Method
def jsonObat(request):
    data=serializers.serialize("json",Obat.objects.all())
    return HttpResponse(data, content_type="application/json")
# Create your views here.
