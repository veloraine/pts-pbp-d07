from django.contrib.auth import models
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import *
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def profile(request):
    return render(request, 'profile.html', {})

@login_required
@csrf_exempt
def edit(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            return redirect('profile')
            # return JsonResponse('sukses')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'edit.html', context)

@csrf_exempt
def userJson(request):
    try:
        userModel = User.objects.get(username = str(request.POST['username']))
        data = {
        'user' : userModel.username,
        'email' : userModel.email,
        'first_name' : userModel.first_name,
        'last_name' : userModel.last_name,
        'bio' : userModel.profile.bio,
        'hobi' : userModel.profile.hobi, 
        'insta' : userModel.profile.insta, 
        'alamat' : userModel.profile.alamat,
    }
    except:
        userModel = User.objects.get(username = 'rasasayange')
        data = {
        'user' : userModel.username,
        'email' : userModel.email,
        'first_name' : userModel.first_name,
        'last_name' : userModel.last_name,
        'bio' : userModel.profile.bio,
        'hobi' : userModel.profile.hobi, 
        'insta' : userModel.profile.insta, 
        'alamat' : userModel.profile.alamat,
    }

    return JsonResponse(data, content_type="application/json")

@csrf_exempt
def PostMethod(request):
    # get user and profile models
    try:
        user = User.objects.get(username = str(request.POST['username']))
        profile = user.profile

        # update and stuff
        user.username = request.POST['username']
        user.email = request.POST['email']
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        profile.bio = request.POST['bio']
        profile.hobi = request.POST['hobi']
        profile.insta = request.POST['insta']
        profile.alamat = request.POST['alamat']

    except:
        user = User.objects.get(username = 'rasasayange')
        profile = user.profile
        
        # update and stuff
        user.username = request.POST['username']
        user.email = request.POST['email']
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        profile.bio = request.POST['bio']
        profile.hobi = request.POST['hobi']
        profile.insta = request.POST['insta']
        profile.alamat = request.POST['alamat']
        
    
    profile.save()
    user.save()



