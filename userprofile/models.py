from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(default = "Default bio given to everyone!")
    hobi = models.CharField(default = "menjelajahi fitur-fitur temancovid", max_length=100)
    insta = models.CharField(default = "-", max_length=50)
    alamat = models.TextField(default= "Jalan di rumahku, istanaku")

    def __str__(self):
        return f'{self.user.username} | Profile'

@receiver(post_save, sender=User)
def profile_create(sender, instance=None, created=False, **kwargs):
    if created:
        Profile.objects.create(user=instance,)