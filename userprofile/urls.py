from django.urls import path
from django.urls.conf import include
from .views import profile, edit, userJson, PostMethod


urlpatterns = [
    path('', profile, name='profile'),
    path('edit', edit, name='edit-profile'),
    path('json', userJson, name='user-json'),
    path('post', PostMethod, name='post')
]