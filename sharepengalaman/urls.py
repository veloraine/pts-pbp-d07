from django.urls import path
from django.urls.conf import include
from .views import postData, story, add_story, jsonStory

urlpatterns = [
    path('', story),
    path('write', add_story),
    path('json', jsonStory),
    path('post', postData)
]