from django import forms
from django.forms import fields
from .models import Story

class StoryForm(forms.ModelForm):
    class Meta:
        model = Story
        fields = "__all__"
        error_messages = {
            'required' : "Please fill the information"
        }
        input_att = {
            'type' : 'text'
        }