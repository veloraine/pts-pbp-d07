from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .models import Story
from .form import StoryForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core import serializers
from register.models import Role 
from django.template.defaultfilters import slugify
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def story(request):
    stories = Story.objects.all()
    return render(request, "story.html", context={
        'stories' : stories
    })

@login_required(login_url='/register/login')
def add_story(request):
    if (Role.roles == "Belum Pernah"):
        messages.info(request, "You are not allowed to share story")
    else:
        if (request.method == "POST"):
            messages.info(request, f"{Role.roles}")
            form = StoryForm(request.POST or None)
            if (form.is_valid()):
                form.save()
                return HttpResponseRedirect('/story')
            else:
                return HttpResponse('Form not valid')
        else:
            form = StoryForm()
            return render(request, "write_stories.html", context={
                'form' : form
            })
@csrf_exempt
def postData(request):
    story = Story(title = request.POST.get("title", None),
    by = request.POST.get("by", None),
    experience = request.POST.get("experience", None))
    story.save()

# JSON response
def jsonStory(request):
    data = serializers.serialize("json", Story.objects.all())
    return HttpResponse(data, content_type="application/json")