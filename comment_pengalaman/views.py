from django.core import serializers
from django.shortcuts import render
from .forms import CommentForm
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from sharepengalaman.models import Story
from .models import Comment

@csrf_exempt
def detail(request, slug):
    story = Story.objects.get(slug=slug)
    commentz = story.comments.all()

    if request.method == 'POST':
        form = CommentForm(request.POST)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.story = story
            obj.save()

            response_data = {}
            response_data['oleh'] = request.POST.get('oleh')
            response_data['body'] = request.POST.get('body')
            return JsonResponse(response_data)


    else:
        form = CommentForm()

    context = {
        'story': story,
        'form': form,
        'commentz': commentz,
    }

    return render(request, 'detail.html', context)

def jsonComment(request, slug):
    data = serializers.serialize("json", Story.objects.get(slug=slug).comments.all())
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def postMethod(request, slug):
    isi = Story.objects.get(slug=slug)
    komen = Comment(
        story = isi,
        body = request.POST.get('body', None),
        oleh = request.POST.get('oleh', None),
    )
    komen.save()