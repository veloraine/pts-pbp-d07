from django.db import models
from django.contrib.auth.models import User
from sharepengalaman.models import Story

class Comment(models.Model):
    story = models.ForeignKey(Story, related_name="comments", on_delete=models.CASCADE, blank=True, null=True)
    body = models.TextField()
    oleh = models.TextField()

    def __str__(self):
        return '%s' %(self.body, self.oleh)

