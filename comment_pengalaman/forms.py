from django import forms
from django.forms import ModelForm
from .models import Comment

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['oleh','body']

        widgets = {
            'body': forms.Textarea(attrs={'class':'form-floating', 'placeholder':'Write a comment...', 'label':'', 'id':'body'}),
            'oleh': forms.TextInput(attrs={'class':'form-floating', 'placeholder':'Write name', 'label':'', 'id':'oleh'}),
        }
        labels = {
            'body':'',
            'oleh':''
        }
