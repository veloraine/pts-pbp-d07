const modal = document.getElementById('simpleModal');
const modalBtn = document.getElementById('modalBtn');
const closebtn = document.getElementsByClassName('closebtn')[0];

modalBtn.addEventListener('click', openModal);
closebtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

function openModal() {
    modal.style.display = 'block';
    console.log(123)
}

function closeModal() {
    modal.style.display = 'none';
}

function outsideClick(e) {
    if (e.target == modal) {
        modal.style.display = 'none';
    }
}
