from django.shortcuts import render
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from .forms import FeedbackForm
from .models import Feedback


def index(request):
    return render(request, 'homepage_index.html')


def feedbacks(request):
    context = {}

    form = FeedbackForm(request.POST)

    context['form'] = form

    return render(request, 'form_feedback.html', context)


@csrf_exempt
def create_post(request):
    form = {}
    if request.method == 'POST':
        form = FeedbackForm(request.POST)

        post_text = request.POST.get('the_post')
        response_data = {}

        post = Feedback(feedback=post_text)
        post.save()

        response_data['feedback'] = post_text

        return JsonResponse(response_data)

    else:
        form = FeedbackForm()

    return render(request, 'form_feedback.html', {'form': form})


def feedbackJson(request):
    feedback = Feedback.objects.all()
    data = serializers.serialize('json', feedback)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def postFeedback(request):
    submit_feedback = Feedback(
        feedback = request.POST.get('feedback', None)
    )

    submit_feedback.save()
