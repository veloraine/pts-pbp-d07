from django.urls import path
from .views import index, feedbacks, create_post, feedbackJson, postFeedback

urlpatterns = [
    path('', index, name='index'),
    #path('feedback/', feedbacks, name='feedback'),
    path('feedback/', create_post, name='create_post'),
    path('feedbackjson/', feedbackJson, name='feedback_json'),
    path('postfeedback/', postFeedback, name='postFeedback')
]
